package
{
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.MouseEvent;
    import flash.text.TextField;
    
    /**
     * Базовый класс отображаемого контейнера.
     */
    public class BaseView extends MovieClip
    {
        public function BaseView()
        {
            super();
        }
        
        /**
         * Инициализация кнопки.
         * @param btn - заданный мувиклип, с вложенными состояниями кнопок
         * @param text - текст кнопки
         * @param clickHandler - обработчик события нажатия
         */
        protected function initButton(btn : MovieClip, text : String = "", clickHandler : Function = null):void
        {
            if (btn == null) {
                return;
            }
            
            btn.buttonMode = true;
            btn.gotoAndStop("up");
            
            var label : TextField = btn["label"] as TextField;
            if (label != null) {
                label.text = text;
                label.mouseEnabled = false;
            }
            
            btn.addEventListener(MouseEvent.CLICK, function (e:MouseEvent):void
            {
                if (btn.currentFrameLabel != "disabled") {
                    btn.gotoAndStop("over");
                    
                    if (clickHandler != null) {
                        clickHandler(e);
                    }
                }
            });
            
            btn.addEventListener(MouseEvent.MOUSE_DOWN, function (e:MouseEvent):void
            {
                if (btn.currentFrameLabel != "disabled") {
                    btn.gotoAndStop("down");
                }
            });
            
            btn.addEventListener(MouseEvent.MOUSE_OVER, function (e:MouseEvent):void
            {
                if (btn.currentFrameLabel != "disabled") {
                    btn.gotoAndStop("over");
                }
            });
            btn.addEventListener(MouseEvent.MOUSE_OUT, function (e:MouseEvent):void
            {
                if (btn.currentFrameLabel != "disabled") {
                    btn.gotoAndStop("up");
                }
            });
        }
    }
}