﻿/*
* Содержит основной класс приложения.
*/

package
{
    import com.CarsController;
    import com.Crossroad;
    import com.FileManager;
    import com.Library;
    import com.Rand;
    import com.Road;
    
    import fl.controls.CheckBox;
    import fl.controls.ComboBox;
    import fl.events.ComponentEvent;
    
    import flash.display.Loader;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Utils3D;
    import flash.net.FileFilter;
    import flash.net.FileReference;
    import flash.net.URLRequest;
    import flash.text.TextField;
    
    /**
     * Основной класс приложения.
     * Класс является точкой входа в приложения.
     */
    public class TrafficFlow extends BaseView
    {
        private static var _INSTANCE : TrafficFlow;
        
        public var mapContainer : Sprite;
        public var map : Map;
        public var infoLabel : TextField;
        public var countLabel : TextField;
        public var timeLabel : TextField;
        public var carsController : CarsController;
        public var settingsPanel : SettingsPanel;
        
        private var dTime : Number;
        
        /** Конструктор. */
        public function TrafficFlow()
        {
            _INSTANCE = this;
            super();
            Rand.init(123);
            
            if (stage == null) {
                addEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
            } else {
                init();
            }
        }
        
        /** Событие при добавлении на сцену. */
        private function onAddedToStage(event : Event):void
        {
            removeEventListener(Event.ADDED_TO_STAGE, onAddedToStage);
            
            init();
        }
        
        public static function getSingleton():TrafficFlow
        {
            return _INSTANCE;
        }
        
        /** Инициализирует начальные данные. */
        private function init() : void
        {
            mapContainer = this["mapContainer"];
            
            map = Library.createMovieClip("Map") as Map;
            mapContainer.addChild(map);
            
            settingsPanel = this["settingsPanelClip"] as SettingsPanel;
            settingsPanel.visible = false;
            initButton(this["loadDataButton"], "Загрузить параметры", onLoadClick);
            initButton(this["saveDataButton"], "Сохранить параметры", onSaveClick);
            initButton(this["settingsButton"], "Настройки", onSettingsClick);
            
            if (this.getChildByName("testButton") != null) {
                initButton(this["testButton"], "Не нажимать", onTestClick);
            }
            
            infoLabel = this["infoTextField"] as TextField;
            infoLabel.text = "Необходимо загрузить файл с параметрами";
            
            countLabel = this["countTextField"] as TextField;
            countLabel.mouseEnabled = false;
            
            timeLabel = this["timeTextField"] as TextField;
            timeLabel.mouseEnabled = false;
            timeLabel.text = "0";
            
            (this["modeCombobox"] as ComboBox).addEventListener(Event.CHANGE, onModeChange);
            (this["checkRoadLength"] as CheckBox).addEventListener(Event.CHANGE, onRoadCheckChange);
            (this["checkLights"] as CheckBox).addEventListener(Event.CHANGE, onCheckLightsChange);
            (this["checkThroughput"] as CheckBox).addEventListener(Event.CHANGE, onCheckThroughputChange);
            (this["dTimeLabel"] as TextField).addEventListener(Event.CHANGE, onDeltaTimeChange);
            
            dTime = Number((this["dTimeLabel"] as TextField).text);
            carsController = new CarsController();
            
            this.addEventListener(Event.ENTER_FRAME, onStep);
            
            onLoadClick(null);
        }
        
        /** Нажатие по кнопке "Загрузить параметры". */
        private function onLoadClick(event:Event):void
        {
            FileManager.loadFile([new FileFilter("Text Files (*.txt)", "*.txt;")], onDataLoaded);
        }
        
        /** Событие по завершению загрузки данных. */
        private function onDataLoaded(event:Event):void
        {
            infoLabel.text = "Выберите режим";
            var s : String = (event.target as FileReference).data.toString();
            map.loadMatrix(s);
            carsController.start();
        }
        
        /** Нажатие по кнопке "Сохранить параметры" */
        private function onSaveClick(event:Event):void
        {
            var matrix : Array = map.matrix;
            var crossroads : Array = map.crossroads;
            
            var i : int;
            var j : int;
            
            var s : String = "";
            
            s += crossroads.length + "\n";
            
            for (i = 0; i < crossroads.length; i++) {
                s += (crossroads[i] as Crossroad).toString() + ";";
            }
            
            s += "\n";
            
            for (i = 0; i < matrix.length; i++) {
                for (j = 0; j < matrix.length; j++) {
                    if ((matrix[i][j] as Road) == null) {
                        s += "0;";
                    } else {
                        s += (matrix[i][j] as Road).toString() + ";";
                    }
                }
                
                s += "\n";
            }
            
            FileManager.saveFile(s);
        }
        
        /** Нажатие по кнопке "Вычислить пути" */
        private function onSettingsClick(event:Event):void
        {
            settingsPanel.show();
        }
        
        /**  */
        private function onTestClick(event:Event):void
        {
            var N : int = map.crossroads.length;
            for (var i : int = 0; i < N; i++) {
                (map.crossroads[i] as Crossroad).tg = 30;
                (map.crossroads[i] as Crossroad).tr = 35;
            }
        }
        
        private function onModeChange(event : Event):void
        {
            map.onModeChange();
            var mode : int = (this["modeCombobox"] as ComboBox).selectedIndex;
            
            infoLabel.text = ["Выберите начальный и конечный перекресток",
                              "Соедините два перекрестка и создайте/удалите дорогу.\nИли редактируйте значения параметров" +
                              "",
                              "Выберите начало и конец пути для создания автомобиля"][mode];
        }
        
        private function onRoadCheckChange(event : Event):void
        {
            map.roadLabels.visible = (event.currentTarget as CheckBox).selected;
            
            if (map.roadLabels.visible) {
                (this["checkThroughput"] as CheckBox).selected = false;
                (this["checkThroughput"] as CheckBox).dispatchEvent(new Event(Event.CHANGE));
            }
        }
        
        private function onCheckThroughputChange(event : Event):void
        {
            map.throughputLabels.visible = (event.currentTarget as CheckBox).selected;
            
            if (map.throughputLabels.visible) {
                (this["checkRoadLength"] as CheckBox).selected = false;
                (this["checkRoadLength"] as CheckBox).dispatchEvent(new Event(Event.CHANGE));
            }
        }
        
        private function onCheckLightsChange(event : Event):void
        {
            map.lightLabels.visible = (event.currentTarget as CheckBox).selected;
        }
        
        private function onDeltaTimeChange(event : Event):void
        {
            try {
                dTime = Number((event.currentTarget as TextField).text);
            } catch (e : Error) {
                trace("Error! Неверный формат!");
            }
        }
        
        private function onStep(event : Event):void
        {
            carsController.step(dTime);
        }
    }
}