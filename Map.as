﻿package
{
    import com.CarsController;
    import com.Crossroad;
    import com.Library;
    import com.Pathfinding;
    import com.Road;
    
    import fl.controls.ComboBox;
    
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.events.TimerEvent;
    import flash.text.TextField;
    import flash.utils.Timer;
    
    /**
     * Класс карты. Отображение + логика взаимодействия с картой.
     */
    public class Map extends MovieClip
    {
        private static var _INSTANCE : Map;
        private static var _DEFAULT_C : int = 1;
        
        public var crossroads : Array;
        public var matrix     : Array;
        public var matrixTime : Array;
        public var pathByTime : Array;
        
        public var matrixLength : Array;
        public var pathByLength : Array;
        
        public var points           : Array;
        public var pointLabels      : Sprite;
        public var throughputLabels : Sprite;
        public var lightLabels      : Sprite;
        public var roadLabels       : Sprite;
        public var carsContainer    : Sprite;
        
        private var firstPoint   : MovieClip;
        private var linesRoot    : Sprite;
        private var selectedRoot : Sprite;
        private var isLoaded     : Boolean;
        
        /***/
        public function Map()
        {
            _INSTANCE = this;
            super();
            
            linesRoot        = this["linesContainer"] as MovieClip;
            selectedRoot     = this["selectedLinesContainer"] as MovieClip;
            
            pointLabels      = this["pointLabelsRoot"] as Sprite;
            throughputLabels = this["throughputLabelsRoot"] as Sprite;
            lightLabels      = this["lightLabelsRoot"] as Sprite;
            roadLabels       = this["roadLabelsRoot"] as Sprite;
            carsContainer    = this["carsRoot"] as Sprite;
            
            selectedRoot.mouseChildren = selectedRoot.mouseEnabled = false;
            pointLabels.mouseChildren = pointLabels.mouseEnabled = false;
            
            throughputLabels.visible = throughputLabels.mouseChildren = throughputLabels.mouseEnabled = false;
            lightLabels.visible = lightLabels.mouseChildren = lightLabels.mouseEnabled = false;
            roadLabels.visible = roadLabels.mouseChildren = roadLabels.mouseEnabled = false;
            carsContainer.mouseChildren = carsContainer.mouseEnabled = false;
            
            isLoaded = false;
            firstPoint = null;
            initMatrix();
        }
        
        public static function getSingleton() : Map
        {
            return _INSTANCE;
        }
        
        private function initMatrix():void
        {
            var i:int;
            var j:int;
            var point : MovieClip;
            
            var pointLabel : TextField;
            points = new Array();
            var label : Sprite;
            crossroads = new Array();
            lightLabels.removeChildren();
            
            i = 0;
            while (true) {
                point = getChildByName("p" + (i + 1)) as MovieClip;
                if (point == null) {
                    break;
                }
                
                points[i] = point;
                point.buttonMode = true;
                pointLabel = point["label"] as TextField;
                
                if (pointLabel != null) {
                    pointLabel.mouseEnabled = false;
                    pointLabel.text = "" + (i + 1);
                    //pointLabel.visible = false;
                }
                
                point.addEventListener(MouseEvent.CLICK, onPointClick);
                
                crossroads[i] = new Crossroad(i);
                
                // Текст/параметры сфетофора
                label = Library.createSprite("EditableLabel");
                label.x = point.x;
                label.y = point.y + label.height;
                label["label"].text = "1;0";
                label.name = "l" + i;
                label["label"].addEventListener(Event.CHANGE, onLightLabelChange);
                lightLabels.addChild(label);
                
                i++;
            }
            
            var count : int = i;
            matrix = new Array();
            matrixTime = new Array();
            pathByTime = new Array();
            matrix.length = matrixTime.length = pathByTime.length = count;
            
            for (i = 0; i < count; i++) {
                matrix[i] = new Array();
                matrixTime[i] = new Array();
                pathByTime[i] = new Array();
                
                (matrix[i] as Array).length = count;
                (matrixTime[i] as Array).length = count;
                (pathByTime[i] as Array).length = count;
                
                for (j = 0; j < count; j++) {
                    matrix[i][j] = null;
                }
            }
        }
        
        public function loadMatrix(s:String):void
        {
            var strLines : Array = s.split("\n");
            
            var count : int = int(strLines[0] as String);
            
            var i : int;
            var j : int;
            
            matrix = new Array();
            
            matrixTime = new Array();
            pathByTime = new Array();
            
            matrixLength = new Array();
            pathByLength = new Array();
            
            matrix.length = matrixTime.length = pathByTime.length = matrixLength.length = pathByLength.length = count;
            
            var arr : Array;
            var L : Number;
            
            crossroads.length = count;
            arr = (strLines[1] as String).split(";");
            for (i = 0; i < count; i++) {
                crossroads[i] = new Crossroad(i);
                (crossroads[i] as Crossroad).fromString(arr[i] as String);
                lightLabels.getChildByName("l" + i)["label"].text = (crossroads[i] as Crossroad).tg + ";" +
                    (crossroads[i] as Crossroad).tr;
            }
            
            for (i = 0; i < count; i++) {
                matrix[i] = new Array();
                matrixTime[i] = new Array();
                pathByTime[i] = new Array();
                matrixLength[i] = new Array();
                pathByLength[i] = new Array();
                
                (matrix[i] as Array).length = (matrixTime[i] as Array).length = (pathByTime[i] as Array).length =
                    (matrixLength[i] as Array).length = (pathByLength[i] as Array).length = count;
                
                arr = (strLines[i + 2] as String).split(";");
                
                for (j = 0; j < count; j++) {
                    L = Number(arr[j] as String);
                    if (arr[j] as String == "0" || arr[j] as String == "") {
                        matrix[i][j] = null;
                    } else {
                        matrix[i][j] = new Road(i * 100 + j, 1, _DEFAULT_C, 0, i, j);
                        (matrix[i][j] as Road).fromString(arr[j] as String);
                    }
                }
            }
            
            linesRoot.removeChildren();
            roadLabels.removeChildren();
            throughputLabels.removeChildren();
            
            var line:Sprite;
            var sprite1:Sprite;
            var sprite2:Sprite;
            var label : Sprite;
            
            for (i = 0; i < count; i++) {
                for (j = i + 1; j < count; j++) {
                    if (matrix[i][j] != null) {
                        sprite1 = points[i];
                        sprite2 = points[j];
                        L = Math.sqrt(Math.pow(sprite2.x - sprite1.x, 2) + Math.pow(sprite2.y - sprite1.y, 2));
                        
                        line = Library.createSprite("Line");
                        line.width = L;
                        line.x = sprite2.x;
                        line.y = sprite2.y;
                        line.name = "line_" + i + "_" + j;
                        line.rotation = Math.atan2(sprite1.y - sprite2.y, sprite1.x - sprite2.x) / Math.PI * 180;
                        linesRoot.addChild(line);
                        
                        // Длина пути
                        label = Library.createSprite("EditableLabel");
                        label.x = (sprite1.x + sprite2.x) * 0.5;
                        label.y = (sprite1.y + sprite2.y) * 0.5;
                        label["label"].text = "" + (matrix[i][j] as Road).getTime();
                        label.name = "l" + i + "_" + j;
                        label["label"].addEventListener(Event.CHANGE, onRoadLengthChange);
                        roadLabels.addChild(label);
                        
                        // Пропускная способность
                        label = Library.createSprite("EditableLabel");
                        label.x = (sprite1.x + sprite2.x) * 0.5;
                        label.y = (sprite1.y + sprite2.y) * 0.5;
                        label["label"].text = (matrix[i][j] as Road).C;
                        label.name = "l" + i + "_" + j;
                        label["label"].addEventListener(Event.CHANGE, onThroughputChange);
                        throughputLabels.addChild(label);
                    }
                }
            }
            
            isLoaded = true;
            
            Pathfinding.fillMatrixMinByTime(matrix, matrixLength);
            Pathfinding.findMatrixMinFloyd(matrix, matrixLength, pathByLength);
            
            // Вычисляем пути
            findMatrixMin();
        }
        
        public function reset():void
        {
            var i:int;
            var j:int;
            var count : int = matrix.length;
            
            for (i = 0; i < count; i++) {
                for (j = i + 1; j < count; j++) {
                    if (matrix[i][j] != null) {
                        (matrix[i][j] as Road).clear();
                        
                        if (matrix[j][i] != null) {
                            (matrix[j][i] as Road).clear();
                            
                            (roadLabels.getChildByName("l" + i + "_" + j)["label"] as TextField).text = "" +
                                Math.round(( Math.max((matrix[j][i] as Road).getTime(), (matrix[i][j] as Road).getTime())));
                        } else {
                            (roadLabels.getChildByName("l" + i + "_" + j)["label"] as TextField).text = "" +
                                Math.round(((matrix[i][j] as Road).getTime()));
                        }
                    }
                }
            }
        }
        
        public function findMatrixMin():void
        {
            if (!isLoaded) {
                return;
            }
            
            Pathfinding.fillMatrixMinByTime(matrix, matrixTime);
            Pathfinding.findMatrixMinFloyd(matrix, matrixTime, pathByTime);
            
            var i : int;
            var j : int;
            var count : int = matrix.length;
            for (i = 0; i < count; i++) {
                for (j = i + 1; j < count; j++) {
                    if (matrix[i][j] != null) {
                        if (matrix[j][i] != null) {
                            (roadLabels.getChildByName("l" + i + "_" + j)["label"] as TextField).text = "" +
                                Math.round(( Math.max((matrix[j][i] as Road).getTime(), (matrix[i][j] as Road).getTime())));
                        } else {
                            (roadLabels.getChildByName("l" + i + "_" + j)["label"] as TextField).text = "" +
                                Math.round(((matrix[i][j] as Road).getTime()));
                        }
                    }
                }
            }
        }
        
        private function tracePath(i:int, j:int):void
        {
            if (!isLoaded || pathByTime.length != matrix.length || (pathByTime[0] as Array).length != matrix.length) {
                return;
            }
            
            var s:String = "" + (j + 1);
            var k:int = j;
            
            selectedRoot.removeChildren();
            
            var selectedLine:Sprite;
            var line:Sprite;
            var oldK : int;
            
            while (k != i) {
                oldK = k;
                k = pathByTime[i][k];
                s = (k + 1) + " -> " + s;
                
                line = getLineClip(k, oldK);
                selectedLine = Library.createSprite("Line");
                selectedLine.x = line.x;
                selectedLine.y = line.y;
                selectedLine.width = getDistanceBetweenPoints(oldK, k);
                selectedLine.rotation = line.rotation;
                selectedRoot.addChild(selectedLine);
            }
            
            trace(s);
            
            TrafficFlow.getSingleton().infoLabel.text = "Длина/время найденного пути: " + Math.round(matrixTime[i][j]);
        }
        
        private function getLineClip(i : int, j : int):Sprite
        {
            var res : Sprite = linesRoot.getChildByName("line_" + i + "_" + j) as Sprite;
            if (res == null) {
                res = linesRoot.getChildByName("line_" + j + "_" + i) as Sprite;
            }
            
            return res;
        }
        
        private function getDistanceBetweenPoints(i:int, j:int):Number
        {
            return Math.sqrt(Math.pow(points[i].x - points[j].x, 2) + Math.pow(points[i].y - points[j].y, 2));
        }
        
        private function onPointClick(event : MouseEvent):void
        {
            if (!isLoaded) {
                return;
            }
            
            var mode : int = (TrafficFlow.getSingleton()["modeCombobox"] as ComboBox).selectedIndex;
            var target : MovieClip = event.currentTarget as MovieClip;
            
            if (firstPoint == null) {
                firstPoint = target;
            } else {
                var a : int = points.indexOf(firstPoint);
                var b : int = points.indexOf(target);
                
                if (a == b) {
                    return;
                }
                
                if (mode == 0) { // Поиск пути
                    tracePath(a, b);
                } else
                if (mode == 1) { // Создание/удаление дороги
                    var line : Sprite;
                    
                    if (matrix[a][b] == null) {
                        var L : Number = Math.sqrt(Math.pow(firstPoint.x - target.x, 2) + Math.pow(firstPoint.y - target.y, 2));
                        
                        matrix[a][b] = new Road(a * 100 + b, Math.round(L), _DEFAULT_C, 0, a, b);
                        matrix[b][a] = new Road(b * 100 + a, Math.round(L), _DEFAULT_C, 0, b, a);
                        
                        line = Library.createSprite("Line");
                        line.width = L;
                        line.x = firstPoint.x;
                        line.y = firstPoint.y;
                        line.name = "line_" + a + "_" + b;
                        line.rotation = Math.atan2(target.y - firstPoint.y, target.x - firstPoint.x) / Math.PI * 180;
                        linesRoot.addChild(line);
                        
                        var label : Sprite;
                        // Длина пути
                        label = Library.createSprite("EditableLabel");
                        label.x = (firstPoint.x + target.x) * 0.5;
                        label.y = (firstPoint.y + target.y) * 0.5;
                        label["label"].text = "" + (matrix[a][b] as Road).getTime();
                        label.name = "l" + Math.min(a, b) + "_" + Math.max(a, b);
                        label["label"].addEventListener(Event.CHANGE, onRoadLengthChange);
                        roadLabels.addChild(label);
                        
                        // Пропускная способность
                        label = Library.createSprite("EditableLabel");
                        label.x = (firstPoint.x + target.x) * 0.5;
                        label.y = (firstPoint.y + target.y) * 0.5;
                        label["label"].text = (matrix[a][b] as Road).C;
                        label.name = "l" + Math.min(a, b) + "_" + Math.max(a, b);
                        label["label"].addEventListener(Event.CHANGE, onThroughputChange);
                        throughputLabels.addChild(label);
                    } else {
                        matrix[a][b] = null;
                        matrix[b][a] = null;
                        
                        line = linesRoot.getChildByName("line_" + a + "_" + b) as Sprite;
                        if (line == null) {
                            line = linesRoot.getChildByName("line_" + b + "_" + a) as Sprite;
                        }
                        
                        if (line != null) {
                            line.parent.removeChild(line);
                        }
                    }
                } else { // Создание новой машины
                    CarsController.getSingleton().createCustomCar(a, b, 3);
                }
                firstPoint = null;
            }
        }
        
        public function onModeChange():void
        {
            firstPoint = null;
            var mode : int = (TrafficFlow.getSingleton()["modeCombobox"] as ComboBox).selectedIndex;
            
            roadLabels.mouseEnabled = roadLabels.mouseChildren = false;//(mode == 1); //Нельзя редактировать длины
            throughputLabels.mouseEnabled = throughputLabels.mouseChildren = (mode == 1);
            lightLabels.mouseEnabled = lightLabels.mouseChildren = (mode == 1);
        }
        
        /** Изменения параметров светофора. */
        private function onLightLabelChange(event : Event):void
        {
            var label : TextField = event.currentTarget as TextField;
            
            try {
                var i : int = int(label.parent.name.substr(1));
                var k : int = label.text.indexOf(";");
                if (k != -1) {
                    var tg = int(label.text.substr(0, k));
                    var tr = int(label.text.substr(k + 1));
                    
                    (crossroads[i] as Crossroad).tg = tg;
                    (crossroads[i] as Crossroad).tr = tr;
                }
            } catch (e : Error) {
                trace("ERROR! Неверно заданы параметры светофора");
            }
        }
        
        /** Изменения длины дороги. */
        private function onRoadLengthChange(event : Event):void
        {
            var label : TextField = event.currentTarget as TextField;
            //"l000_000"
            try {
                var k : int = label.parent.name.indexOf("_");
                if (k != -1) {
                    var i = int(label.parent.name.substring(1, k));
                    var j = int(label.parent.name.substr(k + 1));
                    
                    (matrix[i][j] as Road).L = (matrix[j][i] as Road).L = Number(label.text);
                }
            } catch (e : Error) {
                trace("ERROR! Неверно задана длина дороги");
            }
        }
        
        /** Изменения пропускной способности дороги. */
        private function onThroughputChange(event : Event):void
        {
            var label : TextField = event.currentTarget as TextField;
            //"l000_000"
            try {
                var k : int = label.parent.name.indexOf("_");
                if (k != -1) {
                    var i = int(label.parent.name.substring(1, k));
                    var j = int(label.parent.name.substr(k + 1));
                    
                    (matrix[i][j] as Road).C = (matrix[j][i] as Road).C = Number(label.text);
                }
            } catch (e : Error) {
                trace("ERROR! Неверно задана пропускная способность дороги");
            }
        }
    }
}