package
{
    import com.CarsController;
    import com.Config;
    
    import flash.display.MovieClip;
    import flash.events.Event;
    import flash.text.TextField;
    
    /**
     * Класс окна настроек.
     */
    public class SettingsPanel extends BaseView
    {
        private var countEditLabel : TextField;
        private var percentLabel1 : TextField;
        private var percentLabel2 : TextField;
        
        /** Конструктор. */
        public function SettingsPanel()
        {
            super();
            
            initButton(this["okButton"], "ОК", onOkClick);
            initButton(this["cancelButton"], "Отмена", onCancelClick);
            
            countEditLabel = this["countEditTextField"] as TextField;
            percentLabel1 = this["percentTextField1"] as TextField;
            percentLabel2 = this["percentTextField2"] as TextField;
        }
        
        /** Показывает окно. */
        public function show():void
        {
            visible = true;
            countEditLabel.text = "" + Config.carCount;
            percentLabel1.text = "" + Config.carModelRate1;
            percentLabel2.text = "" + Config.carModelRate2;
        }
        
        /** Нажатие на кнопку ОК. */
        private function onOkClick(event : Event):void
        {
            countEditLabel.textColor = percentLabel1.textColor = percentLabel2.textColor = 0x000000;
            
            var count : int = tryGetInt(countEditLabel);
            var percent1 : Number = tryGetNumber(percentLabel1);
            var percent2 : Number = tryGetNumber(percentLabel2);
            
            if (count < 0 || percent1 < 0 || percent2 < 0) {
                return;
            }
            
            Config.carCount = count;
            Config.carModelRate1 = percent1;
            Config.carModelRate2 = percent2;
            
            visible = false;
            
            CarsController.getSingleton().reset();
            CarsController.getSingleton().start();
        }
        
        private function tryGetInt(label : TextField):int
        {
            var val : Number = parseInt(label.text);
            
            if (isNaN(val)) {
                label.textColor = 0xFF0000;
                return -1;
            }
            
            return val;
        }
        
        private function tryGetNumber(label : TextField):Number
        {
            var val : Number = parseFloat(label.text);
            
            if (isNaN(val)) {
                label.textColor = 0xFF0000;
                return -1;
            }
            
            return val;
        }
        /** Нажатие на кнопку Отмена. */
        private function onCancelClick(event : Event):void
        {
            visible = false;
        }
    }
}