package com
{
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.net.FileReference;
    
    /**
     * Класс для загрузки и сохранения данных.
     */
    public class FileManager
    {
        public static function loadFile(filter:Array, completeHandler : Function, errorHandler : Function = null):void
        {
            var file : FileReference = new FileReference();
            
            file.addEventListener(Event.SELECT, function (e:Event):void {
                file.addEventListener(Event.COMPLETE, completeHandler);
                
                if (errorHandler != null) {
                    file.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
                }
                file.load();
            });
            file.addEventListener(Event.CANCEL, function (e:Event):void {
                if (errorHandler != null) {
                    errorHandler(null);
                }
            });
            
            file.browse(filter);
        }
        
        public static function saveFile(data:String, completeHandler : Function = null,
                                        errorHandler : Function = null):void
        {
            var file : FileReference = new FileReference();
            
            file.addEventListener(Event.SELECT, function (e:Event):void {
                if (completeHandler != null) {
                    file.addEventListener(Event.COMPLETE, completeHandler);
                }
                
                if (errorHandler != null) {
                    file.addEventListener(IOErrorEvent.IO_ERROR, errorHandler);
                }
            });
            file.addEventListener(Event.CANCEL, function (e:Event):void {
                if (errorHandler != null) {
                    errorHandler(null);
                }
            });
            
            file.save(data, "data.txt");
        }
    }

}