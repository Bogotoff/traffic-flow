package com
{
    /** Класс перекрестка. */
    public class Crossroad
    {
        /** Идентификатор перекрестка. */
        public var id : int;
        
        /** Время мигания зеленого сигнала. */
        public var tg : int = 1;
        
        /** Время мигания красного сигнала. */
        public var tr : int = 0;
        
        /** Конструктор. */
        public function Crossroad(id : int, tg : int = 1, tr : int = 0)
        {
            this.id = id;
            this.tg = tg;
            this.tr = tr;
        }
        
        /** Конвертирует в текстовую строку для сохранения. */
        public function toString():String
        {
            return tg + "_" + tr;
        }
        
        /** Загружает переменные из текстовой строки. */
        public function fromString(s : String):void
        {
            var k: int = s.indexOf("_");
            if (k == -1) {
                tg = int(s);
                tr = 0;
            } else {
                tg = int(s.substr(0, k));
                tr = int(s.substr(k + 1));
            }
        }
    }
}