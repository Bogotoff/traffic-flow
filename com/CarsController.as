package com
{
    /**
     * Класс-контроллер, который управляет созданием, удалением и моделированием ТС.
     */
    public class CarsController
    {
        /** Ссылка на экземпляр класса. */
        private static var _INSTANCE : CarsController;
        
        /** Массив индексов начальных пунктов. */
        private static var startCrossroadArr : Array = [64, 76, 75, 62, 4, 3, 10, 11, 12];
        
        /** Массив индексов конечных пунктов. */
        private static var endCrossroadArr   : Array = [55, 29, 28, 26, 24, 6, 9];
        
        /** Карта. */
        private var map : Map;
        /** Счетчик идентификаторов ТС. */
        private var carId : int;
        private var cars : Array;
        private var isStarted : Boolean;
        private var allTime : Number;
        private var oldDTime;
        private var secondInterval;
        private var createCars : Array;
        private var main : TrafficFlow;
        private var carsTime : Number;
        
        /** Конструктор. */
        public function CarsController()
        {
            _INSTANCE = this;
            
            cars = new Array();
            main = TrafficFlow.getSingleton();
            this.map = main.map;
            carId = 0;
            allTime = 0;
            carsTime = 0;
            oldDTime = 0;
            secondInterval = 0;
            isStarted = false;
            createCars = new Array();
            fillCreateCars();
        }
        
        /** Возвращает экземпляр класса. */
        public static function getSingleton() : CarsController
        {
            return _INSTANCE;
        }
        
        /** Удаляет все ТС. */
        public function reset():void
        {
            for (var i : int = 0; i < cars.length; i++) {
                (cars[i] as Car).destroy();
            }
            
            map.reset();
            carId = 0;
            cars.length = 0;
            carsTime = 0;
            allTime = 0;
            oldDTime = 0;
            secondInterval = 0;
            fillCreateCars();
            main.countLabel.text = "Количество ТС: " + cars.length + "\nОбщее кол-во: " + carId;
            main.timeLabel.text = "0";
        }
        
        /** Запускает процесс моделирования. */
        public function start():void
        {
            isStarted = true;
        }
        
        /** Приостанавливает процесс моделирования. */
        public function pause():void
        {
            isStarted = false;
        }
        
        /** Создает массив данных ТС, которые будут создаваться при моделировании. */
        private function fillCreateCars():void
        {
            var i:int;
            var t : Number;
            var a : int;
            var b : int;
            var type : Number;
            Rand.init(Config.carCount);
            
            createCars.length = 0;
            var t1 : Number = 0;
            var t2 : Number = 0;
            var t3 : Number = 0;
            
            for (i = 0; i < Config.carCount; i++) {
                type = (Config.carModelRate1 + Config.carModelRate2) * Rand.getNext();
                
                if (type <= Config.carModelRate1) {
                    type = 1;
                } else
                if (type > Config.carModelRate1 && type <= Config.carModelRate1 + Config.carModelRate2) {
                    type = 2;
                } else {
                    type = 3;
                }
                
                if (i == 0) {
                    t = 0;
                } else {
                    t = createCars[i - 1][0] + 1 / (Config.carModelRate1 + Config.carModelRate2);
                }
                
                a = startCrossroadArr[(int)(startCrossroadArr.length * Rand.getNext())];
                b = endCrossroadArr[(int)(endCrossroadArr.length * Rand.getNext())];
                
                createCars[i] = [t, a, b, (int)(type)];
            }
        }
        
        /**
         * Создает новую машину.
         * 
         * @param startCrossroad - id начального перекрестка
         * @param startCrossroad - id конечного перекрестка
         * @param type - тип авто(1 - по минимальному пути, 2 - по минимальному времени, 3 - )
         */
        public function createCar(startCrossroad : int, endCrossroad : int, type : int = -1, dTime : Number = -1):Car
        {
            var t : int = type;
            if (t == -1) {
                var a : Number = (Config.carModelRate1 + Config.carModelRate2) * Rand.getNext();
                
                if (a <= Config.carModelRate1) {
                    t = 1;
                } else
                if (a > Config.carModelRate1 && a <= Config.carModelRate1 + Config.carModelRate2) {
                    t = 2;
                } else {
                    t = 3;
                }
            }
            
            var car : GraphicCar = new GraphicCar(carId, t, 0, startCrossroad, endCrossroad);
            carId++;
            cars.push(car);
            
            if (dTime < 0) {
                car.step(oldDTime);
            } else {
                car.step(dTime);
            }
            
            main.countLabel.text = "Количество ТС: " + cars.length + "\nОбщее кол-во: " + carId;
            
            return car;
        }
        
        /**
         * Создает новую пользовательскую машину.
         * 
         * @param startCrossroad - id начального перекрестка
         * @param startCrossroad - id конечного перекрестка
         * @param type - тип авто(1 - по минимальному пути, 2 - по минимальному времени, 3 - )
         */
        public function createCustomCar(startCrossroad : int, endCrossroad : int, type : int = -1, dTime : Number = -1):CustomCar
        {
            var t : int = type;
            if (t == -1) {
                var a : Number = (Config.carModelRate1 + Config.carModelRate2) * Rand.getNext();
                
                if (a <= Config.carModelRate1) {
                    t = 1;
                } else
                if (a > Config.carModelRate1 && a <= Config.carModelRate1 + Config.carModelRate2) {
                    t = 2;
                } else {
                    t = 3;
                }
            }
            
            var car : CustomCar = new CustomCar(carId, t, 0, startCrossroad, endCrossroad);
            carId++;
            cars.push(car);
            
            if (dTime < 0) {
                car.step(oldDTime);
            } else {
                car.step(dTime);
            }
            
            main.countLabel.text = "Количество ТС: " + cars.length + "\nОбщее кол-во: " + carId;
            
            return car;
        }
        
        /**
         * Обработка кадра.
         * Моделирование одного шага.
         */
        public function step(dTime : Number):void
        {
            if (!isStarted) {
                return;
            }
            
            oldDTime = dTime;
            if (carId >= Config.carCount && cars.length == 0) {
                isStarted = false;
                var totalTime : Number = (int)(allTime);
                var cTime : Number = (int)(carsTime);
                trace("Симуляция завершена");
                trace("Общее время: " + totalTime);
                trace("Суммарное время всех ТС: " + cTime);
                
                main.infoLabel.text = "Симуляция завершена.\nСуммарное время всех ТС: " + cTime + " сек.";
                
                return;
            }
            
            allTime += dTime;
            secondInterval -= dTime;
            
            main.timeLabel.text = "" + (int)(allTime);
            
            while (secondInterval < 0) {
                secondInterval += 10;
                
                map.findMatrixMin();
            }
            
            var i : int;
            for (i = cars.length - 1; i >= 0; i--) {
                (cars[i] as Car).step(dTime);
            }
            
            if (carId < Config.carCount) {
                while (createCars.length > 0 && createCars[0][0] <= allTime) {
                    createCar(createCars[0][1], createCars[0][2], createCars[0][3], dTime + allTime - createCars[0][0]);
                    createCars.splice(0, 1);
                }
            }
        }
        
        /** Событие срабатывает, когда ТС достигает конечный пункт. */
        public function onCarPathComplete(car : Car):void
        {
            cars.splice(cars.indexOf(car), 1);
            carsTime += car.getTime();
            
            if (cars.length == 0) {
                allTime = car.getEndTime();
            }
            
            main.countLabel.text = "Количество ТС: " + cars.length + "\nОбщее кол-во: " + carId;
        }
        
        /** Возвращает время с момента начала симуляции. */
        public function getTime():Number
        {
            return allTime;
        }
    }
}
