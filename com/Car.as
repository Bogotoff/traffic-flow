package com
{
    import flash.display.Sprite;
    import flash.sampler.NewObjectSample;
    
    /**
     * Класс транспортного средства.
     */
    public class Car
    {
        public static var vMax = 16.66;//60 км/ч = 16.66 м/с
        
        /** Идентификатор автомобиля. */
        public var id : int;
        
        /** Тип модели передвижения. */
        public var type : int;
        
        /** Полное время до следующего перекрестка. */
        public var tf : Number;
        
        /** Оставшееся время до следующего перекрестка. */
        public var td : Number;
        
        /** Текущая дорога. */
        public var Ec : Road;
        
        /** Время начала пути (в секундах). */
        public var tStart : Number;
        
        /** Начальный перекресток. */
        public var Vs : Crossroad;
        
        /** Конечный перекресток. */
        public var Ve : Crossroad;
        
        /** Время завершения. */
        private var tEnd : Number;
        
        /** Предыдущий перекресток. */
        protected var prevCrossroad : Crossroad;
        /** Следующий перекресток. */
        protected var nextCrossroad : Crossroad;
        protected var map : Map;
        
        /** Конструктор. */
        public function Car(id : int, type : int, tStart : Number, Vs : int, Ve : int)
        {
            this.id = id;
            this.type = type;
            this.tStart = tStart;
            map = Map.getSingleton();
            this.Vs = map.crossroads[Vs] as Crossroad;
            this.Ve = map.crossroads[Ve] as Crossroad;
            tEnd = 0;
            prevCrossroad = this.Vs;
        }
        
        public function step(dTime : Number):void
        {
            if (Ec == null) {
                generatePath();
                switchRoad(map.matrix[Vs.id][nextCrossroad.id]);
            }
            td -= dTime;
            
            while (td < 0) {
                var leftTime : Number = -td;
                prevCrossroad = nextCrossroad;
                generatePath();
                
                if (prevCrossroad != nextCrossroad) {
                    switchRoad(map.matrix[prevCrossroad.id][nextCrossroad.id]);
                    td -= leftTime;
                } else {
                    Ec.removeCar(this);
                    tEnd = CarsController.getSingleton().getTime() - leftTime;
                    onPathComplete();
                    break;
                }
            }
        }
        
        protected function onPathComplete():void
        {
            CarsController.getSingleton().onCarPathComplete(this);
        }
        
        private function switchRoad(targetRoad : Road):void
        {
            if (Ec != null) {
                Ec.removeCar(this);
            }
            
            Ec = targetRoad;
            tf = targetRoad.getTime();
            td = tf;
            Ec.addCar(this);
        }
        
        private function generatePath():void
        {
            var i:int = prevCrossroad.id;
            var j:int = Ve.id;
            
            if (i == j) {
                nextCrossroad = prevCrossroad;
                return;
            }
            
            var k:int = j;
            var path : Array;
            
            if (type == 1) {
                path = map.pathByLength;
            } else {
                path = map.pathByTime;
            }
            
            var oldK : int = -1;
            
            while (k != i) {
                oldK = k;
                k = path[i][k];
            }
            
            nextCrossroad = map.crossroads[oldK];
        }
        
        public function destroy():void
        {
            //ничего не делаем
        }
        
        /** Возвращает общее время с момента начала передвижения. */
        public function getTime():Number
        {
            return (tEnd == 0) ? (CarsController.getSingleton().getTime() - tStart) : (tEnd - tStart);
        }
        
        /** Возвращает конечное время. */
        public function getEndTime():Number
        {
            return tEnd;
        }
    }
}