package com
{
    import flash.utils.getTimer;
    
    /** Генератор случайных чисел. */
    public class Rand 
    {
        private static const MAX_RATIO:Number = 1 / uint.MAX_VALUE;
        private static var r : uint = 1;
        
        /**
         * Конструктор. Инициализирует счетчик случайных чисел с начальным значением seed.
         */
        public static function init(seed:uint = 0)
        {
            r = seed;// || getTimer();
        }
        
        /** Возвращает случайное вещественное число в интервале [0, 1). */
        public static function getNext():Number
        {
            r ^= (r << 21);
            r ^= (r >>> 35);
            r ^= (r << 4);
            
            var i : Number = (r % 1000) / 1000;
            return i;
            //return (r * MAX_RATIO);
        }
    }
}