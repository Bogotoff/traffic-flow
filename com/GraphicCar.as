package com
{
    import flash.display.MovieClip;
    import flash.display.Sprite;
    
    /**
     * Класс отображаемого транспортного средства.
     */
    public class GraphicCar extends Car
    {
        /** Отображаемый объект. */
        protected var graphic : MovieClip;
        
        /** Конструктор. */
        public function GraphicCar(id : int, type : int, tStart : Number, Vs : int, Ve : int)
        {
            super(id, type, tStart, Vs, Ve);
            
            initGraphic();
            map.carsContainer.addChild(graphic);
            step(0);
        }
        
        protected function initGraphic():void
        {
            graphic = Library.createMovieClip("CarClip");
            graphic.mouseChildren = graphic.mouseEnabled = false;
            graphic.gotoAndStop(type);
        }
        
        /** Обновление позиции графики в каждом кадре. */
        public override function step(dTime : Number):void
        {
            super.step(dTime);
            
            var p : Number = (tf == 0) ? 1 : 1 - td / tf;
            
            var a : Sprite = map.points[prevCrossroad.id] as Sprite;
            var b : Sprite = map.points[nextCrossroad.id] as Sprite;
            
            graphic.x = lerp(a.x, b.x, p);
            graphic.y = lerp(a.y, b.y, p);
        }
        
        private function lerp(a : Number, b : Number, value : Number) : Number
        {
            return a + (b - a) * value;
        }
        
        protected override function onPathComplete():void
        {
            super.onPathComplete();
            graphic.parent.removeChild(graphic);
        }
        
        public override function destroy():void
        {
            graphic.parent.removeChild(graphic);
        }
    }
}
