package com
{
    import fl.controls.List;
    
    /**
     * Класс для поиска пути.
     */
    public class Pathfinding
    {
        /** Заполняет матрицу matrixMin значениями времени из матрицы дорог matrix. */
        public static function fillMatrixMinByTime(matrix : Array, matrixMin : Array):void
        {
            var i : int;
            var j : int;
            var N : int = matrix.length;
            
            for (i = 0; i < N; i++) {
                for (j = 0; j < N; j++) {
                    matrixMin[i][j] = (i == j) ? 0 : (matrix[i][j] == null) ? Number.POSITIVE_INFINITY :
                        (matrix[i][j] as Road).getTime();
                }
            }
        }
        
        /** Заполняет матрицу matrixMin расстояниями из матрицы дорог matrix. */
        public static function fillMatrixMinByLength(matrix : Array, matrixMin : Array):void
        {
            var i : int;
            var j : int;
            var N : int = matrix.length;
            
            for (i = 0; i < N; i++) {
                for (j = 0; j < N; j++) {
                    matrixMin[i][j] = (i == j) ? 0 : (matrix[i][j] == null) ? Number.POSITIVE_INFINITY :
                        (matrix[i][j] as Road).L;
                }
            }
        }
        
        /** Поиск пути между всеми пунктами алгоритмом Флойда. */
        public static function findMatrixMinFloyd(matrix : Array, matrixMin : Array, path : Array):void
        {
            var i : int;
            var j : int;
            var k : int;
            var N : int = matrix.length;
            
            for (i = 0; i < N; i++) {
                for (j = 0; j < N; j++) {
                    path[i][j] = i;
                }
            }
            
            for (i = 0; i < N; i++) {// будем улучшать путь через i-ю вершину
                // перебираем все пары вершин
                for (j = 0; j < N; j++) {
                    for (k = 0; k < N; k++) {
                        if (matrixMin[j][k] > matrixMin[j][i] + matrixMin[i][k]) {
                            // если пойти из j в i, а из i в k выгоднее, чем пойти напрямую из j в k
                            matrixMin[j][k] = matrixMin[j][i] + matrixMin[i][k];
                            
                            path[j][k] = path[i][k];
                        }
                    }
                }
                // улучшаем путь из j в k
            }
        }
    }
}