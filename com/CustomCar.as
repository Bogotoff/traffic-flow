package com
{
    /**
     * Класс пользовательского транспортного средства.
     */
    public class CustomCar extends GraphicCar
    {
        /** Конструктор. */
        public function CustomCar(id:int, type:int, tStart:Number, Vs:int, Ve:int)
        {
            super(id, type, tStart, Vs, Ve);
        }
        
        protected override function initGraphic():void
        {
            graphic = Library.createMovieClip("CustomCarClip");
            graphic.mouseChildren = graphic.mouseEnabled = false;
            graphic.gotoAndStop(type);
        }
    }
}