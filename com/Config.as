package com
{
    /** Класс для хранения параметров моделирования. */
    public class Config
    {
        /** Количество ТС. */
        public static var carCount : int = 1000;
        /** Интенсивность создания ТС/сек. для модели поведения 1 */
        public static var carModelRate1 : Number = 0.3;
        /** Интенсивность создания ТС/сек. для модели поведения 2 */
        public static var carModelRate2 : Number = 0.3;
    }
}