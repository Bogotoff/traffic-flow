package com
{
    /** Класс дороги. */
    public class Road
    {
        /** Идентификатор дороги. */
        public var id : int;
        
        /** Длина дороги. */
        public var L : Number;
        
        /** Пропускная способность. */
        public var C : Number;
        
        /** Количество автомобилей на дороге. */
        public var K : int = 0;
        
        /** Перекресток в начале дороги. */
        public var Vs : Crossroad;
        
        /** Перекресток в конце дороги. */
        public var Ve : Crossroad;
        
        /** Последний автомобиль в очереди. */
        public var lastCar : Car;
        
        /** Конструктор. */
        public function Road(id : int, L : Number, C : Number, K : int, Vs : int, Ve : int)
        {
            this.id = id;
            this.L = L;
            this.C = C;
            this.K = K;
            this.Vs = Map.getSingleton().crossroads[Vs];
            this.Ve = Map.getSingleton().crossroads[Ve];
        }
        
        /** Конвертирует в текстовую строку для сохранения. */
        public function toString():String
        {
            return L + "_" + C;
        }
        
        /** Загружает переменные из текстовой строки. */
        public function fromString(s : String):void
        {
            var k: int = s.indexOf("_");
            if (k == -1) {
                L = Number(s);
                C = 1;
            } else {
                L = Number(s.substr(0, k));
                C = Number(s.substr(k + 1));
            }
        }
        
        /** Добавление ТС на дорогу. */
        public function addCar(car : Car):void
        {
            lastCar = car;
            K++;
        }
        
        /** Удаление ТС. */
        public function removeCar(car : Car):void
        {
            if (lastCar == car) {
                lastCar = null;
            }
            
            K--;
        }
        
        /** Очищает все ТС на дороге. */
        public function clear():void
        {
            lastCar = null;
            
            K = 0;
        }
        
        /** Возвращает время проезда по дороге. */
        public function getTime():Number
        {
            var lightTime : Number = (Ve.tg + Ve.tr) / (Ve.tg * C);
            var time : Number = L / Car.vMax + lightTime;
            
            if (lastCar == null) {
                return time;
            } else {
                return Math.max(lastCar.td + K / C, time);
            }
        }
    }
}