package com
{
    import flash.display.Bitmap;
    import flash.display.BitmapData;
    import flash.display.DisplayObject;
    import flash.display.DisplayObjectContainer;
    import flash.display.MovieClip;
    import flash.display.Sprite;
    import flash.media.Sound;
    import flash.system.ApplicationDomain;
    import flash.utils.getDefinitionByName;
    
    /**
     * Класс для создания экземпляров классов из библиотеки.
     */
    public class Library
    {
        public static function getClassByName(name : String):Class
        {
            return getDefinitionByName(name) as Class;
        }
        
        private static function createClassReference(symbolName:String, domain:ApplicationDomain = null):Class
        {
            try {
                if (domain == null) {
                    return Class(getDefinitionByName(symbolName));
                }
                
                return Class(domain.getDefinition(symbolName));
            } catch(e:Error) {
                throw (new DefinitionError((("Class '" + symbolName) + "' can not be found in Library."), "LibraryError"));
            }
            
            return null;
        }
        
        /** Создает класс мувиклипа из библиотеки по имени. */
        public static function createMovieClip(symbolName:String, domain:ApplicationDomain = null):MovieClip
        {
            var cl:Class = createClassReference(symbolName, domain) as Class;
            if (cl != null){
                return MovieClip(new (cl)());
            }
            
            return null;
        }
        
        public static function createBitmap(symbolName:String, domain:ApplicationDomain = null):Bitmap
        {
            var cl:Class = createClassReference(symbolName, domain) as Class;
            if (cl != null) {
                return Bitmap(new (cl)(1, 1));
            }
            
            return null;
        }
        
        /** Создает класс спрайта из библиотеки по имени. */
        public static function createSprite(symbolName:String, domain:ApplicationDomain = null):Sprite
        {
            var cl:Class = createClassReference(symbolName, domain) as Class;
            if (cl != null) {
                return Sprite(new (cl)());
            }
            
            return null;
        }
        
        public static function createDisplayObjectContainer(symbolName:String, domain:ApplicationDomain = null):DisplayObjectContainer
        {
            var cl:Class = createClassReference(symbolName, domain) as Class;
            if (cl != null) {
                return DisplayObjectContainer(new (cl)());
            }
            
            return null;
        }
        
        public static function createDisplayObject(symbolName:String, domain:ApplicationDomain = null):DisplayObject
        {
            var cl:Class = createClassReference(symbolName, domain) as Class;
            if (cl != null) {
                return DisplayObject(new (cl)());
            }
            
            return null;
        }
        
        public static function createSound(symbolName:String, domain:ApplicationDomain = null):Sound
        {
            var cl:Class = createClassReference(symbolName, domain) as Class;
            if (cl != null) {
                return Sound(new (cl)());
            }
            
            return null;
        }
        
        public static function createBitmapData(symbolName:String, width:int, height:int, domain:ApplicationDomain = null):BitmapData
        {
            var cl:Class = createClassReference(symbolName, domain) as Class;
            return new (cl)(width, height);
        }
    }
}
